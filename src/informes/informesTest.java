/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package informes;

import Entitats.Persona;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.export.Exporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.view.JasperViewer;



/**
 *
 * @author rvallez
 */
public class informesTest {
    
    public static String reportName = "data/AlumnesFerreria";
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        //Creem una llista
        LinkedList<Persona> llista = new LinkedList<Persona>();
        
        llista.add(new Persona(10,"Marc","Parrot",1982));
        llista.add(new Persona(20,"Mariano","Rajoy",1950));
        llista.add(new Persona(30,"Lionel","Messi",1986));
        llista.add(new Persona(40,"Raul","Vallez",1978));
        llista.add(new Persona(50,"Raul","Gonzalez",1978));
        llista.add(new Persona(60,"Brad","Pitt",1970));
        llista.add(new Persona(70,"Super","Lopez",1950));
        llista.add(new Persona(80,"Klark","Kent",1978));
        llista.add(new Persona(90,"Jim","Kerr",1960));
        
        
        
        try {
            //Carreguem el fitxer .jrxml dissenyat amb el Jasper Report Studio 
            // y el compila en un fitxer .jasper (transforma el xml en un binari)
            JasperCompileManager.compileReportToFile( reportName + ".jrxml");
        
        
            //Omplim aquest fitxer .jasper amb una col·lecció que és just el 
            // contingut que volem mostrar
            //Aquest Jasper es pot crear amb el JasperCompileManager.compileReportToFile
            //O també exportant-lo desde el Jaspersoft STUDIO.
            JasperPrint reportToPrint = JasperFillManager.fillReport(
                    reportName + ".jasper", 
                    null, 
                    new JRBeanCollectionDataSource(llista));

            //Definim un objecte exportador a .docx (Microsoft Word)
            Exporter exporter = new JRDocxExporter();
            
            //Definim que el input serà el document generat (print)
            exporter.setExporterInput(new SimpleExporterInput(reportToPrint));

            //Definim el nom del fitxer de la sortida. En aquest cas es un .docx del Microsoft Word. 
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput("data/exporter.docx"));

            //Fem la exportació i es guarda el fitxer docx
            exporter.exportReport();
            
            
            
            //Exportació per a PDF
            exporter = new JRPdfExporter();
            exporter.setExporterInput(new SimpleExporterInput(reportToPrint));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput("data/exporter.pdf"));
            exporter.exportReport();
            
            
            //Per fer visible a una finestra de la propia finestra jasper
            JasperViewer jasperViewer = new JasperViewer(reportToPrint);
            jasperViewer.setVisible(true);

        } catch (JRException ex) {
            Logger.getLogger(informesTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}

