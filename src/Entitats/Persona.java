/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entitats;

/**
 *
 * @author rvallez
 */
public class Persona {
    
    int id;
    String nom;
    String cognoms;
    int codiPostal;
    
    public Persona(int id, String nom, String cognoms, int codiPostal) {
        this.id = id;
        this.nom = nom;
        this.cognoms = cognoms;
        this.codiPostal = codiPostal;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getCognoms() {
        return cognoms;
    }

    public void setCognoms(String cognoms) {
        this.cognoms = cognoms;
    }

    public int getCodiPostal() {
        return codiPostal;
    }

    public void setCodiPostal(int codiPostal) {
        this.codiPostal = codiPostal;
    }
    
    
    
}
